package com.bdshadow.streetcityquests;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.bdshadow.streetcityquests.quests.CitiesActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        onAuthChangeUpdateUI();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();

        onAuthChangeUpdateUI();
    }

    public void newQuestsButtonClick(View v) {
//        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
//            launchSignInActivityForResult(SIGN_IN_NEW_QUEST_REQUEST_CODE);
//            return;
//        }
        launchCitiesActivity();
    }

    @Override
    protected void onAuthChangeUpdateUI() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        TextView ongoingQuestsTextView = findViewById(R.id.ongoingQuestsTextView);
        TextView finishedQuestsTextView = findViewById(R.id.finishedQuestsTextView);

        if (user == null) {
            ongoingQuestsTextView.setVisibility(View.INVISIBLE);
            finishedQuestsTextView.setVisibility(View.INVISIBLE);
        } else {
            ongoingQuestsTextView.setVisibility(View.VISIBLE);
            finishedQuestsTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    private void launchCitiesActivity() {
        Intent intent = new Intent(this, CitiesActivity.class);
        startActivity(intent);
    }
}
