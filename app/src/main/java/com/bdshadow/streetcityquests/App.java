package com.bdshadow.streetcityquests;

import android.app.Activity;
import android.app.Application;

public class App extends Application {

    private Activity currentActivity = null;

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity) {
        this.currentActivity = mCurrentActivity;
    }
}
