package com.bdshadow.streetcityquests.tools;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class SecureJsonObjectRequest extends JsonObjectRequest {

    public SecureJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        GetTokenResult result = null;
        try {
            result = Tasks.await(FirebaseAuth.getInstance().getCurrentUser().getIdToken(false));
        } catch (ExecutionException | InterruptedException e) {
            throw new AuthFailureError("Something went wrong while retrieving user id token", e);
        }
        params.put(
                "Authorization",
                "Bearer " + result.getToken());

        return params;
    }
}
