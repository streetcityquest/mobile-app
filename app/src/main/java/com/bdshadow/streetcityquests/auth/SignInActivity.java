package com.bdshadow.streetcityquests.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bdshadow.streetcityquests.BaseActivity;
import com.bdshadow.streetcityquests.R;
import com.bdshadow.streetcityquests.tools.SecureJsonArrayRequest;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;
import java.util.List;

public class SignInActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 123;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_in;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);
        findViewById(R.id.get_cities).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        onAuthChangeUpdateUI();
    }

    @Override
    protected void onAuthChangeUpdateUI() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            findViewById(R.id.get_cities).setVisibility(View.INVISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.INVISIBLE);
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.user_name).setVisibility(View.INVISIBLE);
            findViewById(R.id.user_email).setVisibility(View.INVISIBLE);
        } else {
            findViewById(R.id.get_cities).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_in_button).setVisibility(View.INVISIBLE);
            findViewById(R.id.user_name).setVisibility(View.VISIBLE);
            findViewById(R.id.user_email).setVisibility(View.VISIBLE);

            ((TextView) findViewById(R.id.user_name)).setText(user.getDisplayName());
            ((TextView) findViewById(R.id.user_email)).setText(user.getEmail());

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.get_cities:
                getCities();
                break;
        }
    }

    private void getCities() {
        SecureJsonArrayRequest citiesRequest = new SecureJsonArrayRequest(
                Request.Method.GET, SERVER_API_URL + "/cities", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    String resultAllCities = "";
                    for (int i = 0; i < response.length(); i++) {
                        resultAllCities += response.getJSONObject(i).getString("name") + " ";
                    }
                    ((TextView) findViewById(R.id.cities)).setText(resultAllCities);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        getRequestQueue().add(citiesRequest);
    }


    private void signIn() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build());

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    private void signOut() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        onAuthChangeUpdateUI();
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            onAuthChangeUpdateUI();
        }
    }


    // https://firebase.google.com/docs/cloud-messaging/auth-server
//    private static String getAccessToken() throws IOException {
//        GoogleCredential googleCredential = GoogleCredential
//                .fromStream(new FileInputStream("service-account.json"))
//                .createScoped(Arrays.asList(SCOPES));
//        googleCredential.refreshToken();
//        return googleCredential.getAccessToken();
//    }
//    URL url = new URL(BASE_URL + FCM_SEND_ENDPOINT);
//    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//httpURLConnection.setRequestProperty("Authorization", "Bearer " + getAccessToken());
//httpURLConnection.setRequestProperty("Content-Type", "application/json; UTF-8");
//return httpURLConnection;

}
