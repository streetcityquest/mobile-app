package com.bdshadow.streetcityquests.quests;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bdshadow.streetcityquests.BaseActivity;
import com.bdshadow.streetcityquests.R;
import com.bdshadow.streetcityquests.tools.DownloadImageTask;
import com.bdshadow.streetcityquests.tools.SecureJsonArrayRequest;
import com.google.android.flexbox.FlexboxLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

public class CitiesActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cities;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCities();
    }

    @Override
    protected void onAuthChangeUpdateUI() {


    }

    private void getCities() {
        final ProgressBar progressBar = findViewById(R.id.progress_bar);
        JsonArrayRequest citiesRequest = new JsonArrayRequest(
                Request.Method.GET, SERVER_API_URL + "/cities", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    showCities(response);
                    progressBar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        getRequestQueue().add(citiesRequest);
    }

    private void showCities(JSONArray response) throws JSONException {
        FlexboxLayout citiesContainerLayout = findViewById(R.id.cities_container_layout);
        for (int i = 0; i < response.length(); i++) {
            ImageView imageView = new ImageView(this);
            String imageUrl = String.format(response.getJSONObject(i).getString("image"), "s");
            new DownloadImageTask(imageView).execute(SERVER_URL + imageUrl);
            int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 270, getResources().getDisplayMetrics());
            int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 132, getResources().getDisplayMetrics());
            int marginTop = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
            imageView.setLayoutParams(new FlexboxLayout.LayoutParams(width, height));
            imageView.layout(0, marginTop, 0, 0);
            //textView.setText(response.getJSONObject(i).getString("name"));
            imageView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            //textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,35);
            //imageView.setGravity(Gravity.BOTTOM);

//            GradientDrawable gd = new GradientDrawable();
//            gd.setColor(0xFF00FF00); // Changes this drawbale to use a single color instead of a gradient
//            gd.setCornerRadius(5);
//            gd.setStroke(1, 0xFF000000);
//            textView.setBackground(gd);

            citiesContainerLayout.addView(imageView);
        }
    }

}
